/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : fast_upms

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 09/01/2025 20:47:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `pid` bigint NULL DEFAULT 0 COMMENT '父ID,默认为父节点(0.父节点)',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门编码',
  `sort` int NULL DEFAULT NULL COMMENT '排序值',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE,
  INDEX `idx_pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 2, 'admin', '2022-07-18 09:53:47', NULL, NULL, 1, 0, '华夏集团', 'hx', 1);
INSERT INTO `sys_dept` VALUES (2, 1, 'admin', '2022-07-18 09:53:48', NULL, NULL, 1, 1, '华夏集团杭州分公司', 'hx-hz', 1);
INSERT INTO `sys_dept` VALUES (3, 1, 'admin', '2022-07-18 09:53:49', NULL, NULL, 1, 1, '华夏集团上海分公司', 'hx-sh', 2);
INSERT INTO `sys_dept` VALUES (4, 0, 'admin', '2022-07-18 09:53:50', NULL, NULL, 1, 2, '总经办', 'zjb', 1);
INSERT INTO `sys_dept` VALUES (5, 0, 'admin', '2022-07-18 09:53:50', NULL, NULL, 1, 2, '综合管理中心', 'zhglzx', 2);
INSERT INTO `sys_dept` VALUES (6, 0, 'admin', '2022-07-18 09:53:50', NULL, NULL, 1, 2, '运营中心', 'yyzx', 3);
INSERT INTO `sys_dept` VALUES (7, 0, 'admin', '2022-07-18 09:53:50', NULL, NULL, 1, 2, '财务中心', 'cwzx', 4);
INSERT INTO `sys_dept` VALUES (8, 0, 'admin', '2022-07-18 09:53:50', NULL, NULL, 1, 2, '研发中心', 'yfzx', 5);
INSERT INTO `sys_dept` VALUES (9, 0, 'admin', '2022-07-18 09:53:50', NULL, NULL, 1, 2, '采购中心', 'cgzx', 6);
INSERT INTO `sys_dept` VALUES (10, 0, 'admin', '2022-07-18 14:31:50', NULL, NULL, 1, 2, '仓配中心', 'cpzx', 7);
INSERT INTO `sys_dept` VALUES (11, 0, 'admin', '2022-07-18 14:33:11', NULL, NULL, 1, 5, '行政部', 'xzb', 1);
INSERT INTO `sys_dept` VALUES (12, 0, 'admin', '2022-07-18 14:33:28', NULL, NULL, 1, 5, '人事部', 'rsb', 2);
INSERT INTO `sys_dept` VALUES (13, 0, 'admin', '2022-07-18 14:33:41', NULL, NULL, 1, 6, '销售部', 'xsb', 1);
INSERT INTO `sys_dept` VALUES (14, 0, 'admin', '2022-07-18 14:33:56', NULL, NULL, 1, 6, '客服部', 'kfb', 2);
INSERT INTO `sys_dept` VALUES (15, 0, 'admin', '2022-07-18 14:34:03', NULL, NULL, 1, 7, '财务部', 'cwb', 1);
INSERT INTO `sys_dept` VALUES (16, 0, 'admin', '2022-07-18 14:34:25', NULL, NULL, 1, 8, '产品部', 'cpb', 1);
INSERT INTO `sys_dept` VALUES (17, 0, 'admin', '2022-07-18 14:34:30', NULL, NULL, 1, 8, '技术部', 'jsb', 2);
INSERT INTO `sys_dept` VALUES (18, 0, 'admin', '2022-07-18 14:38:30', NULL, NULL, 1, 8, '测试部', 'csb', 3);
INSERT INTO `sys_dept` VALUES (19, 0, 'admin', '2022-07-18 14:39:12', NULL, NULL, 1, 8, '运维部', 'ywb', 4);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `type` int NULL DEFAULT 0 COMMENT '日志类型,默认正常(0.正常 1.异常)',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志标题',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ip',
  `cname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '城市',
  `device` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备',
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作系统',
  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '浏览器',
  `network_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '网络类型',
  `service` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '服务名',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'url',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求方式',
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '载荷',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '响应状态',
  `time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '响应时长',
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '异常信息',
  `referer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '来源',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_perm
-- ----------------------------
DROP TABLE IF EXISTS `sys_perm`;
CREATE TABLE `sys_perm`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `pid` bigint NULL DEFAULT 0 COMMENT '父ID,默认为父节点(0.父节点)',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限编码',
  `sort` int NULL DEFAULT NULL COMMENT '排序值',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `spread` int NULL DEFAULT 0 COMMENT '展开,默认不展开(0.不展开 1.展开)',
  `disabled` int NULL DEFAULT 0 COMMENT '状态,默认启用(0.启用 1.禁用)',
  `is_interface` int NULL DEFAULT 0 COMMENT '是否为接口,默认否(0.否 1.是)',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '前端url地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '前端组件地址',
  `ref` int NULL DEFAULT 0 COMMENT '是否为外部菜单，默认否(0.否 1.是)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE,
  INDEX `idx_pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统权限表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_perm
-- ----------------------------
INSERT INTO `sys_perm` VALUES (1, 1, 'admin', '2022-06-17 17:41:02', 'admin', '2022-09-18 01:55:32', 1, 0, '系统设置', 'sys-setting', 1, 'layui-icon-set-fill', 1, 0, 0, '', NULL, 0);
INSERT INTO `sys_perm` VALUES (2, 1, 'admin', '2022-06-17 17:43:44', 'admin', '2022-09-18 01:51:44', 1, 1, '租户管理', 'sys-tenant', 1, 'layui-icon-user', 0, 0, 0, '/views/sys/tenant/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (3, 1, 'admin', '2022-06-17 17:46:23', 'admin', '2022-09-18 01:40:26', 1, 1, '部门管理', 'sys-dept', 2, 'layui-icon-app', 0, 0, 0, '/views/sys/dept/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (4, 1, 'admin', '2022-06-17 17:47:52', 'admin', '2022-09-18 01:52:17', 1, 1, '岗位管理', 'sys-post', 3, 'layui-icon-table', 0, 0, 0, '/views/sys/post/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (5, 1, 'admin', '2022-06-17 17:48:12', 'admin', '2022-09-18 01:51:35', 1, 1, '用户管理', 'sys-user', 4, 'layui-icon-username', 0, 0, 0, '/views/sys/user/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (6, 1, 'admin', '2022-06-17 17:48:18', 'admin', '2022-09-18 01:43:08', 1, 1, '角色管理', 'sys-role', 5, 'layui-icon-vercode', 0, 0, 0, '/views/sys/role/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (7, 1, 'admin', '2022-06-17 17:48:29', 'admin', '2022-09-18 01:55:56', 1, 1, '权限管理', 'sys-perm', 6, 'layui-icon-component', 0, 0, 0, '/views/sys/perm/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (8, 1, 'admin', '2022-06-17 17:49:12', 'admin', '2022-09-18 01:54:22', 1, 1, '日志管理', 'sys-log', 7, 'layui-icon-read', 0, 0, 0, '', NULL, 0);
INSERT INTO `sys_perm` VALUES (9, 1, 'admin', '2022-06-17 17:49:25', 'admin', '2022-10-15 00:45:36', 1, 8, '登录日志', 'sys-log-login', 1, 'layui-icon-log', 0, 0, 0, '/views/sys/log/login/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (10, 1, 'admin', '2022-06-17 17:49:35', 'admin', '2022-10-15 00:45:48', 1, 8, '操作日志', 'sys-log-operate', 2, 'layui-icon-log', 0, 0, 0, '/views/sys/log/operate/list.html', NULL, 0);
INSERT INTO `sys_perm` VALUES (11, 1, 'admin', '2022-10-15 00:46:44', 'admin', '2022-10-15 00:47:02', 1, 0, '我的博客', 'my-blog', 2, 'layui-icon-login-wechat', 0, 0, 0, 'https://blog.csdn.net/weixin_41763571', NULL, 0);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位编码',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 0, 'admin', '2022-09-13 22:32:33', '', NULL, 1, '董事长', 'dsz');
INSERT INTO `sys_post` VALUES (2, 0, 'admin', '2022-09-13 22:32:44', '', NULL, 1, '总裁', 'zc');
INSERT INTO `sys_post` VALUES (3, 0, 'admin', '2022-09-13 22:32:52', '', NULL, 1, '副总裁', 'fzc');
INSERT INTO `sys_post` VALUES (4, 0, 'admin', '2022-09-13 22:33:09', '', NULL, 1, '行政主管', 'xzzg');
INSERT INTO `sys_post` VALUES (5, 0, 'admin', '2022-09-13 22:33:17', '', NULL, 1, '行政前台', 'xzqt');
INSERT INTO `sys_post` VALUES (6, 0, 'admin', '2022-09-13 22:34:30', '', NULL, 1, '司机', 'sj');
INSERT INTO `sys_post` VALUES (7, 0, 'admin', '2022-09-13 22:34:40', '', NULL, 1, '保洁', 'bj');
INSERT INTO `sys_post` VALUES (8, 0, 'admin', '2022-09-13 22:34:48', '', NULL, 1, '招聘主管', 'zpzg');
INSERT INTO `sys_post` VALUES (9, 0, 'admin', '2022-09-13 22:34:56', '', NULL, 1, '招聘专员', 'zpzy');
INSERT INTO `sys_post` VALUES (10, 0, 'admin', '2022-09-13 22:35:48', '', NULL, 1, '销售主管', 'xszg');
INSERT INTO `sys_post` VALUES (11, 0, 'admin', '2022-09-13 22:36:02', '', NULL, 1, '销售专员', 'xszy');
INSERT INTO `sys_post` VALUES (12, 0, 'admin', '2022-09-13 22:36:32', '', NULL, 1, '客户经理', 'khjl');
INSERT INTO `sys_post` VALUES (13, 0, 'admin', '2022-09-13 22:36:40', '', NULL, 1, '客户专员', 'khzy');
INSERT INTO `sys_post` VALUES (14, 0, 'admin', '2022-09-13 22:36:57', '', NULL, 1, '财务总监', 'cwzj');
INSERT INTO `sys_post` VALUES (15, 0, 'admin', '2022-09-13 22:37:09', '', NULL, 1, '会计', 'kj');
INSERT INTO `sys_post` VALUES (16, 0, 'admin', '2022-09-13 22:37:17', '', NULL, 1, '出纳', 'cn');
INSERT INTO `sys_post` VALUES (17, 0, 'admin', '2022-09-13 22:37:51', '', NULL, 1, '审计', 'sj');
INSERT INTO `sys_post` VALUES (18, 0, 'admin', '2022-09-13 22:53:45', '', NULL, 1, '产品总监', 'cpzj');
INSERT INTO `sys_post` VALUES (19, 0, 'admin', '2022-09-13 22:53:59', '', NULL, 1, '产品经理', 'cpjl');
INSERT INTO `sys_post` VALUES (20, 0, 'admin', '2022-09-13 22:54:08', '', NULL, 1, '技术总监', 'jszj');
INSERT INTO `sys_post` VALUES (21, 0, 'admin', '2022-09-13 22:54:18', '', NULL, 1, '架构师', 'jgs');
INSERT INTO `sys_post` VALUES (22, 0, 'admin', '2022-09-13 22:54:27', '', NULL, 1, '前端开发', 'qdkf');
INSERT INTO `sys_post` VALUES (23, 0, 'admin', '2022-09-13 22:54:35', '', NULL, 1, '后端开发', 'hdkf');
INSERT INTO `sys_post` VALUES (24, 0, 'admin', '2022-09-13 22:54:41', '', NULL, 1, '测试主管', 'cszg');
INSERT INTO `sys_post` VALUES (25, 0, 'admin', '2022-09-13 22:54:51', '', NULL, 1, '测试', 'cs');
INSERT INTO `sys_post` VALUES (26, 0, 'admin', '2022-09-13 22:55:03', '', NULL, 1, '运维主管', 'ywzg');
INSERT INTO `sys_post` VALUES (27, 0, 'admin', '2022-09-13 22:55:09', '', NULL, 1, '运维', 'yw');
INSERT INTO `sys_post` VALUES (28, 0, 'admin', '2022-09-13 22:55:16', '', NULL, 1, '采购经理', 'cgjl');
INSERT INTO `sys_post` VALUES (29, 0, 'admin', '2022-09-13 22:55:25', '', NULL, 1, '采购专员', 'cgzy');
INSERT INTO `sys_post` VALUES (30, 0, 'admin', '2022-09-13 22:55:33', '', NULL, 1, '仓库主管', 'ckzg');
INSERT INTO `sys_post` VALUES (31, 0, 'admin', '2022-09-13 22:55:41', '', NULL, 1, '仓库管理员', 'ckgly');
INSERT INTO `sys_post` VALUES (32, 0, 'admin', '2022-09-13 22:56:00', '', NULL, 1, '配送主管', 'pszg');
INSERT INTO `sys_post` VALUES (33, 0, 'admin', '2022-09-13 22:56:13', '', NULL, 1, '配送员', 'psy');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 0, 'admin', '2022-06-17 17:04:37', NULL, NULL, 1, '超级管理员', 'admin');
INSERT INTO `sys_role` VALUES (2, 0, 'admin', '2022-06-17 17:04:48', NULL, NULL, 1, '客服', 'normal');
INSERT INTO `sys_role` VALUES (3, 0, 'admin', '2022-12-15 17:19:09', NULL, NULL, 1, '部门主管', 'deptManager');

-- ----------------------------
-- Table structure for sys_role_perm
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_perm`;
CREATE TABLE `sys_role_perm`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `role_id` bigint NOT NULL COMMENT '系统角色ID',
  `perm_id` bigint NOT NULL COMMENT '系统权限ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE,
  INDEX `idx_role_id`(`role_id`) USING BTREE,
  INDEX `idx_perm_id`(`perm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统角色权限关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_perm
-- ----------------------------
INSERT INTO `sys_role_perm` VALUES (1, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 1);
INSERT INTO `sys_role_perm` VALUES (2, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 2);
INSERT INTO `sys_role_perm` VALUES (3, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 3);
INSERT INTO `sys_role_perm` VALUES (4, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 4);
INSERT INTO `sys_role_perm` VALUES (5, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 5);
INSERT INTO `sys_role_perm` VALUES (6, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 6);
INSERT INTO `sys_role_perm` VALUES (7, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 7);
INSERT INTO `sys_role_perm` VALUES (8, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 8);
INSERT INTO `sys_role_perm` VALUES (9, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 9);
INSERT INTO `sys_role_perm` VALUES (10, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 10);
INSERT INTO `sys_role_perm` VALUES (11, 0, 'admin', '2022-12-16 15:19:59', NULL, NULL, 1, 1, 11);

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '租户名',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uk_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统租户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES (1, 0, 'admin', '2022-09-13 23:15:46', NULL, NULL, '测试租户1');
INSERT INTO `sys_tenant` VALUES (2, 0, 'admin', '2022-09-13 23:15:58', NULL, NULL, '测试租户2');
INSERT INTO `sys_tenant` VALUES (3, 8, 'admin', '2022-09-13 23:16:02', NULL, NULL, '测试租户3');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户openid',
  `unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户unionid',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像地址',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `sex` int NULL DEFAULT 0 COMMENT '性别,默认男(0.男 1.女)',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '住址',
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `id_card_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '身份证号',
  `status` int NULL DEFAULT 0 COMMENT '账号状态,默认正常(0.正常 1.停用)',
  `last_login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后登录ip',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_username`(`username`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE,
  INDEX `idx_phone`(`phone`) USING BTREE,
  INDEX `idx_age`(`age`) USING BTREE,
  INDEX `idx_real_name`(`real_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '超级管理员', 'http://dummyimage.com/100x100', '13812345678', 'p.uxeahimv@qq.com', 28, 0, '浙江省杭州市', '超级管理员', '330000000000000001', 0, '', NULL, 2);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `user_id` bigint NOT NULL COMMENT '系统用户ID',
  `post_id` bigint NOT NULL COMMENT '系统角色ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_post_id`(`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 0, 'admin', '2022-12-12 13:51:37', NULL, NULL, 1, 1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁版本号',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `user_id` bigint NOT NULL COMMENT '系统用户ID',
  `role_id` bigint NOT NULL COMMENT '系统角色ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 0, 'admin', '2022-12-12 13:51:37', NULL, NULL, 1, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
