/** The Web UI Theme-v1.8.0 */
;layui.extend({setter: "config", admin: "lib/admin", view: "lib/view"}).define(["setter", "admin"], function (a) {
    var l = layui.setter, s = layui.element, o = layui.admin, r = o.tabsPage, e = layui.view, u = "layadmin-layout-tabs", y = layui.$;
    y(window);
    o.screen() < 2 && o.sideFlexible(), layui.config({base: l.base + "modules/"}), layui.each(l.extend, function (a, e) {
        var i = {};
        i[l.extend.constructor === Array ? e : a] = "{/}" + l.base + "lib/extend/" + e, layui.extend(i)
    }), e().autoRender(), layui.use("common"), a("index", {
        openTabsPage: function (e, a) {
            function i() {
                s.tabChange(u, e), o.tabsBodyChange(r.index, {url: e, text: a})
            }

            var n, t = y("#LAY_app_tabsheader>li"), d = e.replace(/(^http(s*):)|(\?[\s\S]*$)/g, "");
            t.each(function (a) {
                y(this).attr("lay-id") === e && (n = !0, r.index = a)
            }), a = a || "\u65b0\u6807\u7b7e\u9875";
            l.pageTabs ? n || (setTimeout(function () {
                y("#LAY_app_body").append(['<div class="layadmin-tabsbody-item layui-show">', '<iframe src="' + e + '" frameborder="0" class="layadmin-iframe"></iframe>', "</div>"].join("")), i()
            }, 10), r.index = t.length, s.tabAdd(u, {title: "<span>" + a + "</span>", id: e, attr: d})) : o.tabsBody(o.tabsPage.index).find(".layadmin-iframe")[0].contentWindow.location.href = e, i()
        }
    })
});