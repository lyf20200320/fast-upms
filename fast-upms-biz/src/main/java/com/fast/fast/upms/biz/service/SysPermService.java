package com.fast.fast.upms.biz.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysPerm;
import com.fast.fast.upms.api.vo.SysPermVO;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_perm(系统权限表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysPermService extends IService<SysPerm> {

    /**
     * 新增或更新
     *
     * @param sysPerm
     * @return
     */
    @Override
    boolean saveOrUpdate(SysPerm sysPerm);

    /**
     * 权限树
     *
     * @return
     */
    List<SysPermVO> permTree();

    /**
     * 添加根节点
     *
     * @param sysPerm
     * @return
     */
    boolean addRoot(SysPerm sysPerm);

    /**
     * 添加子节点
     *
     * @param treeNode
     * @return
     */
    boolean addChild(JSONObject treeNode);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean delete(List<?> ids);
}
