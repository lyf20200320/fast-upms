package com.fast.fast.upms.biz.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.fast.upms.api.entity.SysRole;
import com.fast.fast.upms.api.entity.SysRolePerm;
import com.fast.fast.upms.api.entity.SysUserRole;
import com.fast.fast.upms.api.vo.SysRoleVO;
import com.fast.fast.upms.biz.base.util.ClearAuthUtils;
import com.fast.fast.upms.biz.mapper.SysRoleMapper;
import com.fast.fast.upms.biz.service.SysRolePermService;
import com.fast.fast.upms.biz.service.SysRoleService;
import com.fast.fast.upms.biz.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author lyf
 * @description 针对表【sys_role(系统角色表)】的数据库操作Service实现
 * @createDate 2022/01/01 00:00 周六
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole>
        implements SysRoleService {

    @Autowired
    private SysRolePermService sysRolePermService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private ClearAuthUtils clearAuthUtils;

    @Override
    public boolean saveOrUpdate(SysRole sysRole) {
        super.saveOrUpdate(sysRole);
        // 获取该角色下的所有用户id
        List<Long> userIds = sysUserRoleService.list(Wrappers.lambdaQuery(SysUserRole.class)
                .in(ObjectUtil.isNotEmpty(sysRole.getId()), SysUserRole::getRoleId, sysRole.getId())
        ).stream().map(SysUserRole::getUserId).collect(Collectors.toList());
        // 清除指定用户的角色编码集合
        clearAuthUtils.clearSpecifiedUserRoleList(userIds);
        // 清除指定用户的权限编码集合
        clearAuthUtils.clearSpecifiedUserPermList(userIds);
        return true;
    }

    @Override
    public List<String> listRoleCodesByUserId(Long userId) {
        if (ObjectUtil.isEmpty(userId)) {
            return CollUtil.newArrayList();
        }

        // 根据userId,获取roleIds
        List<Long> roleIds = sysUserRoleService.list(
                Wrappers.lambdaQuery(SysUserRole.class)
                        .eq(SysUserRole::getUserId, userId)
        ).stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        if (ObjectUtil.isEmpty(roleIds)) {
            return CollUtil.newArrayList();
        }

        // 根据roleIds,获取roleCodes
        List<String> roleCodes = list(
                Wrappers.lambdaQuery(SysRole.class)
                        .in(CollUtil.isNotEmpty(roleIds), SysRole::getId, roleIds)
        ).stream().map(SysRole::getCode).filter(Objects::nonNull).collect(Collectors.toList());

        return roleCodes;
    }

    @Override
    public List<SysRoleVO> listRoles() {
        List<SysRoleVO> sysRoleVos = list().stream().map(o -> {
            SysRoleVO sysRoleVO = BeanUtil.copyProperties(o, SysRoleVO.class);
            sysRoleVO.setValue(o.getId());
            return sysRoleVO;
        }).collect(Collectors.toList());
        return sysRoleVos;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean delete(List<?> ids) {
        removeBatchByIds(ids);
        if (ObjUtil.isNotEmpty(ids)) {
            // 删除系统用户角色关联表数据
            List<SysUserRole> sysUserRoles = sysUserRoleService.list(Wrappers.lambdaQuery(SysUserRole.class)
                    .in(SysUserRole::getRoleId, ids)
            );
            List<Long> sysUserRoleIds = sysUserRoles.stream().map(SysUserRole::getId).collect(Collectors.toList());
            sysUserRoleService.removeBatchByIds(sysUserRoleIds);

            // 删除系统角色权限关联表数据
            List<Long> sysRolePermIds = sysRolePermService.list(Wrappers.lambdaQuery(SysRolePerm.class)
                    .in(SysRolePerm::getRoleId, ids)
            ).stream().map(SysRolePerm::getId).collect(Collectors.toList());
            sysRolePermService.removeBatchByIds(sysRolePermIds);

            // 获取该角色下的所有用户id
            List<Long> sysUserIds = sysUserRoles.stream().map(SysUserRole::getUserId).collect(Collectors.toList());
            // 清除指定用户的角色编码集合
            clearAuthUtils.clearSpecifiedUserRoleList(sysUserIds);
            // 清除指定用户的权限编码集合
            clearAuthUtils.clearSpecifiedUserPermList(sysUserIds);
        }
        return true;
    }

    @Override
    public List<SysRole> listByUserId(Long userId) {
        List<Long> sysRoleIds = sysUserRoleService.list(Wrappers.lambdaQuery(SysUserRole.class)
                .eq(SysUserRole::getUserId, userId)
        ).stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        if (ObjectUtil.isNotEmpty(sysRoleIds)) {
            List<SysRole> sysRoles = list(Wrappers.lambdaQuery(SysRole.class)
                    .in(SysRole::getId, sysRoleIds)
            );
            return sysRoles;
        }
        return CollUtil.newArrayList();
    }
}




