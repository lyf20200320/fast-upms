package com.fast.fast.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.fast.upms.api.entity.SysLog;
import com.fast.fast.upms.biz.mapper.SysLogMapper;
import com.fast.fast.upms.biz.service.SysLogService;
import org.springframework.stereotype.Service;

/**
 * @author lyf
 * @description 针对表【sys_log(系统日志表)】的数据库操作Service实现
 * @createDate 2022/01/01 00:00 周六
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog>
        implements SysLogService {

}




