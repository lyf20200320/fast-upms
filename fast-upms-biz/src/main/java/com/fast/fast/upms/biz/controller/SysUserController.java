package com.fast.fast.upms.biz.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.upms.api.entity.SysUser;
import com.fast.fast.upms.api.feign.SysUserFeign;
import com.fast.fast.upms.biz.base.enums.FileType;
import com.fast.fast.upms.biz.base.enums.UploadPathConstants;
import com.fast.fast.upms.biz.base.util.UploadUtil;
import com.fast.fast.upms.biz.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 系统用户Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统用户")
@RequestMapping("sysUser")
@RestController
public class SysUserController implements SysUserFeign {

    @Autowired
    private SysUserService sysUserService;

    @Override
    @ApiOperation("新增或更新")
    @PostMapping("mySaveOrUpdate")
    public Result<?> mySaveOrUpdate(@Validated SysUser po) {
        Long id = sysUserService.mySaveOrUpdate(po);
        return id != null ? Result.ok(id) : Result.error();
    }

    @Override
    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysUser>> page(SysUser po) {
        return Result.ok(sysUserService.myPage(po));
    }

    @Override
    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysUser> get(Long id) {
        return Result.ok(sysUserService.getOne(Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getId, id)));
    }

    @Override
    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<Long> ids) {
        return sysUserService.delete(ids) ? Result.ok() : Result.error();
    }

    @Override
    @ApiOperation("根据用户名和密码获取")
    @PostMapping("getByUsernameAndPwd")
    public Result<SysUser> getByUsernameAndPwd(SysUser po) {
        return Result.ok(sysUserService.getByUsernameAndPwd(po));
    }

    @Override
    @ApiOperation("设置账号状态")
    @PostMapping("setStatus")
    public Result<?> setStatus(SysUser po) {
        return sysUserService.setStatus(po) ? Result.ok() : Result.error();
    }

    @Override
    @ApiOperation("修改个人密码")
    @PostMapping("updatePassword")
    public Result<?> updatePassword(SysUser po) {
        return sysUserService.updatePassword(po) ? Result.ok() : Result.error();
    }

    @Override
    @ApiOperation("全部查询")
    @GetMapping("listAll")
    public Result<List<SysUser>> listAll() {
        return Result.ok(sysUserService.list(Wrappers.lambdaQuery(SysUser.class)
                        .orderByAsc(SysUser::getUsername)
                )
        );
    }

    @ApiOperation("上传")
    @PostMapping("upload")
    public Result<?> upload(MultipartFile file) {
        JSONObject jo = UploadUtil.upload(file, UploadPathConstants.USER_AVATAR_IMAGE, FileType.IMAGE);
        if (ObjectUtil.isEmpty(jo)) {
            return Result.error();
        }
        return Result.ok(jo);
    }

}
