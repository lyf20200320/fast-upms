package com.fast.fast.upms.biz.base.util;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import com.fast.fast.common.base.constant.CacheConstants;
import com.fast.fast.common.redis.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 清除用户权限工具类
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Component
public class ClearAuthUtils {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 清除登录用户的角色编码集合
     *
     * @return
     */
    public boolean clearLoginUserRoleList() {
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_ROLE_CODE_USERID + "::" + StpUtil.getLoginId());
        return true;
    }

    /**
     * 清除指定用户的角色编码集合
     *
     * @param userIds
     * @return
     */
    public boolean clearSpecifiedUserRoleList(List<?> userIds) {
        if (ObjectUtil.isEmpty(userIds)) {
            return false;
        }
        userIds.forEach(o -> {
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_ROLE_CODE_USERID + "::" + o);
        });
        return true;
    }

    /**
     * 清除所有用户的角色编码集合
     *
     * @return
     */
    public boolean clearAllUserRoleList() {
        redisUtils.deleteByPrefix(CacheConstants.GATEWAY_LOGIN_SYS_ROLE_CODE_USERID + "::");
        return true;
    }


    // ********************************************************************************************************************************************************************************


    /**
     * 清除登录用户的权限编码集合
     *
     * @return
     */
    public boolean clearLoginUserPermList() {
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_CODE_USERID + "::" + StpUtil.getLoginId());
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_USERID + "::" + StpUtil.getLoginId());
        return true;
    }

    /**
     * 清除指定用户的权限编码集合
     *
     * @param userIds
     * @return
     */
    public boolean clearSpecifiedUserPermList(List<?> userIds) {
        if (ObjectUtil.isEmpty(userIds)) {
            return false;
        }
        userIds.forEach(o -> {
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_CODE_USERID + "::" + o);
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_USERID + "::" + o);
        });
        return true;
    }

    /**
     * 清除所有用户的权限编码集合
     *
     * @return
     */
    public boolean clearAllUserPermList() {
        redisUtils.deleteByPrefix(CacheConstants.GATEWAY_LOGIN_SYS_PERM_CODE_USERID + "::");
        redisUtils.deleteByPrefix(CacheConstants.GATEWAY_LOGIN_SYS_PERM_USERID + "::");
        return true;
    }

    // ********************************************************************************************************************************************************************************

    /**
     * 清除登录用户的登录信息
     *
     * @return
     */
    public boolean clearLoginUserLoginInfo() {
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_USER_USERID_PREFIX + StpUtil.getLoginId());
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_ROLE_CODE_USERID + "::" + StpUtil.getLoginId());
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_CODE_USERID + "::" + StpUtil.getLoginId());
        redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_USERID + "::" + StpUtil.getLoginId());
        return true;
    }

    /**
     * 清除指定用户的登录信息
     *
     * @param userIds
     * @return
     */
    public boolean clearSpecifiedUserLoginInfo(List<?> userIds) {
        userIds.forEach(o -> {
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_USER_USERID_PREFIX + o);
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_ROLE_CODE_USERID + "::" + o);
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_CODE_USERID + "::" + o);
            redisUtils.delete(CacheConstants.GATEWAY_LOGIN_SYS_PERM_USERID + "::" + o);
        });
        return true;
    }
}
