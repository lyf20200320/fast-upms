package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.upms.api.entity.SysUser;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_user(系统用户表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 新增或更新
     *
     * @param sysUser
     * @return
     */
    Long mySaveOrUpdate(SysUser sysUser);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean delete(List<Long> ids);

    /**
     * 根据用户名和密码获取
     *
     * @param sysUser
     * @return
     */
    SysUser getByUsernameAndPwd(SysUser sysUser);

    /**
     * 设置账号状态
     *
     * @param sysUser
     * @return
     */
    boolean setStatus(SysUser sysUser);

    /**
     * 修改个人密码
     *
     * @param sysUser
     * @return
     */
    boolean updatePassword(SysUser sysUser);

    /**
     * 分页查询
     *
     * @param po
     * @return
     */
    PageResult<SysUser> myPage(SysUser po);
}
