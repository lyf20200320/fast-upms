package com.fast.fast.upms.biz.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysDept;
import com.fast.fast.upms.api.vo.SysDeptVO;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_dept(系统部门表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 新增或更新
     *
     * @param sysDept
     * @return
     */
    @Override
    boolean saveOrUpdate(SysDept sysDept);

    /**
     * 部门树
     *
     * @return
     */
    List<SysDeptVO> deptTree();

    /**
     * 部门树2
     *
     * @return
     */
    List<SysDeptVO> deptTree2();

    /**
     * 添加根节点
     *
     * @param sysDept
     * @return
     */
    boolean addRoot(SysDept sysDept);

    /**
     * 添加子节点
     *
     * @param treeNode
     * @return
     */
    boolean addChild(JSONObject treeNode);

}
