package com.fast.fast.upms.biz.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.fast.common.base.exception.BizException;
import com.fast.fast.upms.api.entity.SysUserPost;
import com.fast.fast.upms.biz.mapper.SysUserPostMapper;
import com.fast.fast.upms.biz.service.SysUserPostService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lyf
 * @description 针对表【sys_user_post(系统用户与岗位关联表)】的数据库操作Service实现
 * @createDate 2022/01/01 00:00 周六
 */
@Service
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPost>
        implements SysUserPostService {


    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addPosts(Long userId, List<?> postIds) {
        if (ObjectUtil.isEmpty(userId)) {
            throw new BizException("用户id不能为空");
        }

        // 删除系统用户与岗位关联表数据
        List<Long> ids = list(Wrappers.lambdaQuery(SysUserPost.class)
                .eq(SysUserPost::getUserId, userId)
        ).stream().map(SysUserPost::getId).collect(Collectors.toList());
        removeBatchByIds(ids);

        List<SysUserPost> sysUserPosts = postIds.stream().map(o -> {
            SysUserPost sysUserPost = new SysUserPost();
            sysUserPost.setUserId(userId);
            sysUserPost.setPostId(Long.valueOf((String) o));
            return sysUserPost;
        }).collect(Collectors.toList());
        saveBatch(sysUserPosts);

        return true;
    }
}




