package com.fast.fast.upms.biz.controller;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.common.mybatis.plus.service.SuperService;
import com.fast.fast.upms.api.entity.SysRole;
import com.fast.fast.upms.api.vo.SysRoleVO;
import com.fast.fast.upms.biz.service.SysRoleService;
import com.fast.fast.upms.biz.service.impl.SysRoleServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统角色Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统角色")
@RequestMapping("sysRole")
@RestController
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SuperService<SysRole, ?> superService;

    @ApiOperation("新增或更新")
    @PostMapping("saveOrUpdate")
    public Result<?> saveOrUpdate(@Validated SysRole po) {
        return sysRoleService.saveOrUpdate(po) ? Result.ok() : Result.error();
    }

    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysRole>> page(SysRole po) {
        superService.setClazz(SysRoleServiceImpl.class);
        return Result.ok(superService.page(po, Wrappers.lambdaQuery(SysRole.class)
                        .likeRight(ObjectUtils.isNotEmpty(po.getName()), SysRole::getName, po.getName())
                        .likeRight(ObjectUtils.isNotEmpty(po.getCode()), SysRole::getCode, po.getCode())
                )
        );
    }

    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysRole> get(Long id) {
        return Result.ok(sysRoleService.getOne(Wrappers.lambdaQuery(SysRole.class).eq(SysRole::getId, id)));
    }

    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<?> ids) {
        return sysRoleService.delete(ids) ? Result.ok() : Result.error();
    }

    @ApiOperation("获取用户拥有的角色编码集合")
    @GetMapping("listRoleCodesByUserId")
    public Result<List<String>> listRoleCodesByUserId(Long userId) {
        if (userId == null) {
            return Result.error("userId不能为空");
        }
        return Result.ok(sysRoleService.listRoleCodesByUserId(userId));
    }

    @ApiOperation("获取角色列表")
    @GetMapping("listRoles")
    public Result<List<SysRoleVO>> listRoles() {
        return Result.ok(sysRoleService.listRoles());
    }

}
