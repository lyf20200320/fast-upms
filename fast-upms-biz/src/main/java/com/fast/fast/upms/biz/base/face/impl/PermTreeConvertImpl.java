package com.fast.fast.upms.biz.base.face.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.fast.fast.upms.api.entity.SysPerm;
import com.fast.fast.upms.api.vo.SysPermVO;
import com.fast.fast.upms.biz.base.face.TreeConvert;
import com.fast.fast.upms.biz.base.face.enums.TreeEnum;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 权限树出参转换实现类
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Component
public class PermTreeConvertImpl implements TreeConvert<List<SysPerm>, List<SysPermVO>> {

    /**
     * 权限树出参转换，需符合前端组件dtree的数据格式
     *
     * @param sysPerms 权限集合
     * @return
     */
    @Override
    public List<SysPermVO> convert(List<SysPerm> sysPerms) {
        List<SysPermVO> sysPermVos = sysPerms.stream().map(o -> {
            SysPermVO sysPermVO = BeanUtil.copyProperties(o, SysPermVO.class, "children");
            sysPermVO.setParentId(o.getPid());
            sysPermVO.setTitle(o.getName());
            sysPermVO.setSpread(true);
            JSONObject checkArr = new JSONObject();
            checkArr.put("checkArr", "{\"type\": \"0\", \"checked\": \"0\"}");
            sysPermVO.setCheckArr(checkArr);
            if (o.getChildren() == null) {
                o.setChildren(new ArrayList<>());
            }
            sysPermVO.setChildren(this.convert(o.getChildren()));
            return sysPermVO;
        }).collect(Collectors.toList());
        return sysPermVos;
    }

    @Override
    public List<SysPermVO> convert2(List<SysPerm> sysPerms) {
        return null;
    }

    @Override
    public TreeEnum type() {
        return TreeEnum.PERM_TREE;
    }
}
