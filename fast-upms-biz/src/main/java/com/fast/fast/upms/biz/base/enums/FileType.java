package com.fast.fast.upms.biz.base.enums;

import cn.hutool.core.util.ObjUtil;
import com.fast.fast.common.base.exception.BizException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 文件类型枚举
 *
 * @author lyf
 * @date 2024年10月14日 15点50分
 */
public enum FileType {

    /**
     * 文档
     */
    DOC(
            new ArrayList<>(
                    Arrays.asList(
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                            "application/pdf",
                            "text/plain",
                            "text/plain; charset=UTF-8"
                    )
            ),
            new ArrayList<>(Arrays.asList("doc", "docx", "xls", "xlsx", "ppt", "pptx", "pdf", "txt"))
    ),

    /**
     * 音频
     */
    AUDIO(
            new ArrayList<>(Collections.singletonList("audio/")),
            new ArrayList<>(Arrays.asList("mp3", "wma", "flac", "aac", "mmf", "amr", "m4a", "ogg", "mp2", "wav"))
    ),

    /**
     * 视频
     */
    VIDEO(
            new ArrayList<>(Collections.singletonList("video/")),
            new ArrayList<>(Arrays.asList("3gp", "flv", "mpg", "vob", "mp4", "mkv", "mov", "avi", "wmv", "rmvb"))
    ),

    /**
     * 图片
     */
    IMAGE(
            new ArrayList<>(Collections.singletonList("image/")),
            new ArrayList<>(Arrays.asList("jpg", "jpeg", "png", "ico", "bmp", "gif", "webp", "tif", "tiff"))
    );

    /**
     * 文件内容类型
     */
    private final List<String> contentTypes;

    /**
     * 文件格式
     */
    private final List<String> formats;

    FileType(List<String> contentTypes, List<String> formats) {
        this.contentTypes = contentTypes;
        this.formats = formats;
    }

    /**
     * 判断文件内容类型是否安全
     *
     * @param contentType 文件内容类型
     * @param fileTypes   文件类型枚举
     * @return
     */
    public static boolean isSafeContent(String contentType, FileType... fileTypes) {
        if (ObjUtil.isEmpty(contentType)) {
            throw new BizException("文件内容类型不能为空");
        }
        if (ObjUtil.isEmpty(fileTypes)) {
            return DOC.contentTypes.contains(contentType) || contentType.startsWith(AUDIO.contentTypes.get(0))
                    || contentType.startsWith(VIDEO.contentTypes.get(0)) || contentType.startsWith(IMAGE.contentTypes.get(0));
        }
        for (FileType fileType : fileTypes) {
            if (fileType == DOC) {
                if (DOC.contentTypes.contains(contentType)) {
                    return true;
                }
            }
            if (contentType.startsWith(fileType.contentTypes.get(0))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断文件格式是否安全
     *
     * @param contentType 文件内容类型
     * @param format      文件格式
     * @return
     */
    public static boolean isSafeFormat(String contentType, String format) {
        if (ObjUtil.isEmpty(contentType)) {
            throw new BizException("文件内容类型不能为空");
        }
        if (ObjUtil.isEmpty(format)) {
            throw new BizException("文件格式不能为空");
        }
        return getByContentType(contentType).formats.contains(format);
    }

    public static FileType getByContentType(String contentType) {
        return Arrays.stream(FileType.values())
                .filter(o -> {
                    if (o == DOC) {
                        return DOC.contentTypes.contains(contentType);
                    }
                    return contentType.startsWith(o.contentTypes.get(0));
                })
                .findFirst()
                .orElse(null);
    }

    public static FileType getByFormat(String format) {
        return Arrays.stream(FileType.values())
                .filter(o -> o.formats.contains(format))
                .findFirst()
                .orElse(null);
    }

}
