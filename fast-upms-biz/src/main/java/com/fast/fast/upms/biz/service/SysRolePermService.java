package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysRolePerm;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_role_perm(系统角色权限关联表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysRolePermService extends IService<SysRolePerm> {

    /**
     * 给角色添加权限
     *
     * @param roleId
     * @param permIds
     * @return
     */
    boolean addPerms(Long roleId, List<?> permIds);
}
