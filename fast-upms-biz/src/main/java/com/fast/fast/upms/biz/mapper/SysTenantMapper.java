package com.fast.fast.upms.biz.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.fast.upms.api.entity.SysTenant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_tenant(系统租户表)】的数据库操作Mapper
 * @createDate 2022/01/01 00:00 周六
 * @Entity com.fast.fast.upms.api.entity.SysTenant
 */
@Mapper
public interface SysTenantMapper extends BaseMapper<SysTenant> {

    /**
     * 忽略租户，获取当前数据库中所有的表名
     *
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    List<String> listDBTableNames();

    /**
     * 忽略租户，为该租户删除所有数据
     *
     * @param tableNames
     * @param tenantId
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    boolean removeAllDataForTenantId(@Param("tableNames") List<String> tableNames, @Param("tenantId") Long tenantId);

}




