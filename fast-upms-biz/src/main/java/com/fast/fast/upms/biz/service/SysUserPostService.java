package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysUserPost;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_user_post(系统用户与岗位关联表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysUserPostService extends IService<SysUserPost> {

    /**
     * 给用户添加岗位
     *
     * @param userId
     * @param postIds
     * @return
     */
    boolean addPosts(Long userId, List<?> postIds);
}
