package com.fast.fast.upms.biz.util;

import cn.hutool.core.util.ObjectUtil;
import com.fast.fast.common.base.exception.BizException;
import com.fast.fast.common.core.util.SpringContextUtils;
import com.fast.fast.common.mybatis.plus.context.CustomContext;
import com.fast.fast.upms.api.entity.SysTenant;
import com.fast.fast.upms.biz.service.SysTenantService;
import org.springframework.beans.factory.annotation.Value;

/**
 * 多租户工具类
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/

public class TenantUtils {

    @Value("${tenant.open:false}")
    private boolean tenantOpen;

    /**
     * 检查租户id是否存在于数据库中
     *
     * @return
     */
    public boolean checkTenantId() {
        // 当关闭多租户模式时,放行
        if (!tenantOpen) {
            return true;
        }
        // 获取当前请求头中的租户id
        CustomContext customContext = SpringContextUtils.getBean(CustomContext.class);
        Long currentTenantId = customContext.getCurrentTenantId();
        if (ObjectUtil.isEmpty(currentTenantId)) {
            throw new BizException("非法操作,该租户id不存在");
        }
        // 检查该租户id是否存在于数据库中
        SysTenantService sysTenantService = SpringContextUtils.getBean(SysTenantService.class);
        SysTenant sysTenant = sysTenantService.getById(currentTenantId);
        if (sysTenant == null) {
            throw new BizException("非法操作,该租户id不存在");
        }
        return true;
    }

}
