package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysLog;

/**
 * @author lyf
 * @description 针对表【sys_log(系统日志表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysLogService extends IService<SysLog> {

}
