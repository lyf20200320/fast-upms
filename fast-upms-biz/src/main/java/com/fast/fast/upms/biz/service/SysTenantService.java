package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysTenant;
import com.fast.fast.upms.api.vo.SysTenantVO;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_tenant(系统租户表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysTenantService extends IService<SysTenant> {

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean delete(List<Long> ids);

    /**
     * 获取租户列表
     *
     * @return
     */
    List<SysTenantVO> listTenants();

    /**
     * 根据用户id、租户id，初始化新租户数据库数据
     *
     * @param userId   用户id
     * @param tenantId 租户id
     * @return
     */
    boolean initNewTenantDBDataByUserIdAndTenantId(Long userId, Long tenantId);

}
