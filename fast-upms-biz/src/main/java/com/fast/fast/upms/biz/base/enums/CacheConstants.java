package com.fast.fast.upms.biz.base.enums;

/**
 * 缓存的key常量
 *
 * @author lyf
 * @date 2023年12月29日 14点43分
 **/
public interface CacheConstants {

    Long DAY = 86400L;
    Long WEEK = 86400L * 7;
    Long MONTH = 86400L * 30;
    Long FOREVER = -1L;

}
