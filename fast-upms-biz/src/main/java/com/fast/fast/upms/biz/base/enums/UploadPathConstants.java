package com.fast.fast.upms.biz.base.enums;

/**
 * 上传路径常量
 *
 * @author lyf
 * @date 2024年10月11日 15点59分
 **/
public interface UploadPathConstants {

    String BASE = "/File/upms";

    /**
     * 用户头像图片
     */
    String USER_AVATAR_IMAGE = BASE + "/user/avatar/image";

}
