package com.fast.fast.upms.biz.base.face;

import com.fast.fast.upms.biz.base.face.enums.TreeEnum;

/**
 * 树出参转换接口
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
public interface TreeConvert<T, R> {

    /**
     * 树出参转换
     *
     * @param t 函数参数
     * @return 函数结果
     */
    R convert(T t);

    /**
     * 树出参转换2
     *
     * @param t 函数参数
     * @return 函数结果
     */
    R convert2(T t);

    /**
     * 树枚举
     * <p>
     * 0.权限树
     * 1.部门树
     *
     * @return
     */
    TreeEnum type();

}
