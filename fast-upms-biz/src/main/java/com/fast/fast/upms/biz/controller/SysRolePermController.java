package com.fast.fast.upms.biz.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.upms.api.entity.SysRolePerm;
import com.fast.fast.upms.biz.service.SysRolePermService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统角色权限Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统角色权限")
@RequestMapping("sysRolePerm")
@RestController
public class SysRolePermController {

    @Autowired
    private SysRolePermService sysRolePermService;

    @ApiOperation("根据roleId获取permIds")
    @GetMapping("listPermIdsByRoleId")
    public Result<?> listPermIdsByRoleId(Long roleId) {
        List<Long> permIds = sysRolePermService.list(Wrappers.lambdaQuery(SysRolePerm.class)
                        .eq(ObjectUtil.isNotEmpty(roleId), SysRolePerm::getRoleId, roleId))
                .stream().map(SysRolePerm::getPermId).collect(Collectors.toList());
        return Result.ok(permIds);
    }

    @ApiOperation("给角色添加权限")
    @PostMapping("addPerms")
    public Result<?> addPerms(Long roleId, @RequestParam List<?> permIds) {
        return sysRolePermService.addPerms(roleId, permIds) ? Result.ok() : Result.error();
    }

}
