package com.fast.fast.upms.biz.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.upms.api.entity.SysUserPost;
import com.fast.fast.upms.biz.service.SysUserPostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统用户岗位Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统用户岗位")
@RequestMapping("sysUserPost")
@RestController
public class SysUserPostController {

    @Autowired
    private SysUserPostService sysUserPostService;

    @ApiOperation("根据userId获取postIds")
    @GetMapping("listPostIdsByUserId")
    public Result<?> listPostIdsByUserId(Long userId) {
        List<Long> postIds = sysUserPostService.list(Wrappers.lambdaQuery(SysUserPost.class)
                        .eq(ObjectUtil.isNotEmpty(userId), SysUserPost::getUserId, userId))
                .stream().map(SysUserPost::getPostId).collect(Collectors.toList());
        return Result.ok(postIds);
    }

    @ApiOperation("给用户添加岗位")
    @PostMapping("addPosts")
    public Result<?> addPosts(Long userId, @RequestParam List<?> postIds) {
        return sysUserPostService.addPosts(userId, postIds) ? Result.ok() : Result.error();
    }

}
