package com.fast.fast.upms.biz.base.face.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 树枚举
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Getter
@RequiredArgsConstructor
public enum TreeEnum {

    /**
     * 权限树
     */
    PERM_TREE(0, "权限树"),

    /**
     * 部门树
     */
    DEPT_TREE(1, "部门树");

    /**
     * 树类型
     */
    private final Integer code;

    /**
     * 提示信息
     */
    private final String msg;

    /**
     * 通过树类型获取枚举对象
     *
     * @param code 树类型
     * @return 枚举对象
     */
    public static TreeEnum getByCode(Integer code) {
        for (TreeEnum value : TreeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

    /**
     * 通过树类型获取提示信息
     *
     * @param code 树类型
     * @return 提示信息
     */
    public static String getMsgByCode(Integer code) {
        for (TreeEnum value : TreeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value.getMsg();
            }
        }
        return null;
    }

}
