package com.fast.fast.upms.biz.base.factory;

import com.fast.fast.upms.biz.base.face.TreeConvert;
import com.fast.fast.upms.biz.base.face.enums.TreeEnum;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 树出参转换工厂类
 * <p>
 * 使用策略模式和工厂模式实现
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Component
public class TreeConvertFactory implements ApplicationContextAware {


    private final Map<TreeEnum, TreeConvert> map = new ConcurrentHashMap<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, TreeConvert> tempMap = applicationContext.getBeansOfType(TreeConvert.class);
        tempMap.values().forEach(treeConvert -> map.put(treeConvert.type(), treeConvert));
    }

    /**
     * 获取树出参转换器
     *
     * @param treeEnum 树枚举
     * @return
     */
    public <T, R> TreeConvert<T, R> getConvert(TreeEnum treeEnum) {
        return map.get(treeEnum);
    }

}
