package com.fast.fast.upms.biz.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.upms.api.entity.SysUserRole;
import com.fast.fast.upms.biz.service.SysUserRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统用户角色Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统用户角色")
@RequestMapping("sysUserRole")
@RestController
public class SysUserRoleController {

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @ApiOperation("根据userId获取roleIds")
    @GetMapping("listRoleIdsByUserId")
    public Result<?> listRoleIdsByUserId(Long userId) {
        List<Long> roleIds = sysUserRoleService.list(Wrappers.lambdaQuery(SysUserRole.class)
                        .eq(ObjectUtil.isNotEmpty(userId), SysUserRole::getUserId, userId))
                .stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        return Result.ok(roleIds);
    }

    @ApiOperation("给用户添加角色")
    @PostMapping("addRoles")
    public Result<?> addRoles(Long userId, @RequestParam List<?> roleIds) {
        return sysUserRoleService.addRoles(userId, roleIds) ? Result.ok() : Result.error();
    }

}
