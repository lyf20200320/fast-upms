package com.fast.fast.upms.biz.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fast.fast.upms.api.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author lyf
 * @description 针对表【sys_user(系统用户表)】的数据库操作Mapper
 * @createDate 2022/01/01 00:00 周六
 * @Entity com.fast.fast.upms.api.entity.SysUser
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 忽略租户,根据用户名获取用户
     *
     * @param sysUser
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    SysUser getByUsername(SysUser sysUser);

    /**
     * 忽略租户,根据用户名和密码获取用户
     *
     * @param sysUser
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    SysUser getByUsernameAndPwd(SysUser sysUser);

    Page<SysUser> myPage(@Param("page") Page<SysUser> page, @Param("dto") SysUser dto);

}




