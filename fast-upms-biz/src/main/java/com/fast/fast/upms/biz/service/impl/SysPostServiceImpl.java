package com.fast.fast.upms.biz.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.fast.upms.api.entity.SysPost;
import com.fast.fast.upms.api.entity.SysUserPost;
import com.fast.fast.upms.api.vo.SysPostVO;
import com.fast.fast.upms.biz.mapper.SysPostMapper;
import com.fast.fast.upms.biz.service.SysPostService;
import com.fast.fast.upms.biz.service.SysUserPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lyf
 * @description 针对表【sys_post(系统岗位信息表)】的数据库操作Service实现
 * @createDate 2022/01/01 00:00 周六
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost>
        implements SysPostService {

    @Autowired
    private SysUserPostService sysUserPostService;

    @Override
    public List<SysPostVO> listPosts() {
        List<SysPostVO> sysPostVos = list().stream().map(o -> {
            SysPostVO sysPostVO = BeanUtil.copyProperties(o, SysPostVO.class);
            sysPostVO.setValue(o.getId());
            return sysPostVO;
        }).collect(Collectors.toList());
        return sysPostVos;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean delete(List<?> ids) {
        removeBatchByIds(ids);
        if (ObjUtil.isNotEmpty(ids)) {
            // 删除系统用户与岗位关联表数据
            List<Long> sysUserPostIds = sysUserPostService.list(Wrappers.lambdaQuery(SysUserPost.class)
                    .in(SysUserPost::getPostId, ids)
            ).stream().map(SysUserPost::getId).collect(Collectors.toList());
            sysUserPostService.removeBatchByIds(sysUserPostIds);
        }
        return true;
    }

}




