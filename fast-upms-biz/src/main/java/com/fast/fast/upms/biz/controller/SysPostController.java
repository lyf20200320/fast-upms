package com.fast.fast.upms.biz.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.common.mybatis.plus.service.SuperService;
import com.fast.fast.upms.api.entity.SysPost;
import com.fast.fast.upms.api.vo.SysPostVO;
import com.fast.fast.upms.biz.service.SysPostService;
import com.fast.fast.upms.biz.service.impl.SysPostServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统岗位Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统岗位")
@RequestMapping("sysPost")
@RestController
public class SysPostController {

    @Autowired
    private SysPostService sysPostService;

    @Autowired
    private SuperService<SysPost, ?> superService;

    @ApiOperation("新增或更新")
    @PostMapping("saveOrUpdate")
    public Result<?> saveOrUpdate(@Validated SysPost po) {
        return sysPostService.saveOrUpdate(po) ? Result.ok() : Result.error();
    }

    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysPost>> page(SysPost po) {
        superService.setClazz(SysPostServiceImpl.class);
        return Result.ok(superService.page(po, Wrappers.lambdaQuery(SysPost.class)
                .likeRight(ObjectUtil.isNotEmpty(po.getName()), SysPost::getName, po.getName())
                .likeRight(ObjectUtil.isNotEmpty(po.getCode()), SysPost::getCode, po.getCode()))
        );
    }

    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysPost> get(Long id) {
        return Result.ok(sysPostService.getOne(Wrappers.lambdaQuery(SysPost.class).eq(SysPost::getId, id)));
    }

    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<?> ids) {
        return sysPostService.delete(ids) ? Result.ok() : Result.error();
    }

    @ApiOperation("获取岗位列表")
    @GetMapping("listPosts")
    public Result<List<SysPostVO>> listPosts() {
        return Result.ok(sysPostService.listPosts());
    }
}
