package com.fast.fast.upms.biz.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.common.mybatis.plus.service.SuperService;
import com.fast.fast.upms.api.entity.SysDept;
import com.fast.fast.upms.api.vo.SysDeptVO;
import com.fast.fast.upms.biz.service.SysDeptService;
import com.fast.fast.upms.biz.service.impl.SysDeptServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统部门Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统部门")
@RequestMapping("sysDept")
@RestController
public class SysDeptController {

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SuperService<SysDept, ?> superService;

    private final String DEPT_TREE_REDIS_KEY = "upms:sysDept:deptTree";
    private final String KEY_GENERATOR = "keyGenerator";

    @CacheEvict(value = DEPT_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("新增或更新")
    @PostMapping("saveOrUpdate")
    public Result<?> saveOrUpdate(@Validated SysDept po) {
        return sysDeptService.saveOrUpdate(po) ? Result.ok() : Result.error("父节点不能是自己");
    }

    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysDept>> page(SysDept po) {
        superService.setClazz(SysDeptServiceImpl.class);
        return Result.ok(superService.page(po, Wrappers.lambdaQuery(SysDept.class)
                        .likeRight(ObjectUtils.isNotEmpty(po.getName()), SysDept::getName, po.getName())
                )
        );
    }

    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysDept> get(Long id) {
        return Result.ok(sysDeptService.getOne(Wrappers.lambdaQuery(SysDept.class).eq(SysDept::getId, id)));
    }

    @CacheEvict(value = DEPT_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<?> ids) {
        return sysDeptService.removeBatchByIds(ids) ? Result.ok() : Result.error();
    }

    // 若是开启多租户，则不能使用该缓存
    //@Cacheable(value = DEPT_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("部门树")
    @PostMapping("deptTree")
    public JSONObject deptTree() {
        JSONObject rs = new JSONObject();
        rs.put("status", JSON.parse("{\"code\":200,\"message\":\"操作成功\"}"));
        rs.put("data", sysDeptService.deptTree());
        return rs;
    }

    @ApiOperation("部门树2")
    @PostMapping("deptTree2")
    public Result<List<SysDeptVO>> deptTree2() {
        return Result.ok(sysDeptService.deptTree2());
    }

    @CacheEvict(value = DEPT_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("添加根节点")
    @PostMapping("addRoot")
    public Result<?> addRoot(@Validated SysDept po) {
        return sysDeptService.addRoot(po) ? Result.ok() : Result.error();
    }

    @CacheEvict(value = DEPT_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("添加子节点")
    @PostMapping("addChild")
    public Result<?> addChild(@RequestBody JSONObject treeNode) {
        return sysDeptService.addChild(treeNode) ? Result.ok() : Result.error();
    }

}
