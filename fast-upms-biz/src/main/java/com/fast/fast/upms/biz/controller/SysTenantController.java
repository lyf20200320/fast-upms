package com.fast.fast.upms.biz.controller;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.common.mybatis.plus.service.SuperService;
import com.fast.fast.upms.api.entity.SysTenant;
import com.fast.fast.upms.api.vo.SysTenantVO;
import com.fast.fast.upms.biz.service.SysTenantService;
import com.fast.fast.upms.biz.service.impl.SysTenantServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统租户Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统租户")
@RequestMapping("sysTenant")
@RestController
public class SysTenantController {

    @Autowired
    private SysTenantService sysTenantService;

    @Autowired
    private SuperService<SysTenant, ?> superService;

    @ApiOperation("新增或更新")
    @PostMapping("saveOrUpdate")
    public Result<?> saveOrUpdate(@Validated SysTenant po) {
        return sysTenantService.saveOrUpdate(po) ? Result.ok() : Result.error();
    }

    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysTenant>> page(SysTenant po) {
        superService.setClazz(SysTenantServiceImpl.class);
        return Result.ok(superService.page(po, Wrappers.lambdaQuery(SysTenant.class)
                        .likeRight(ObjectUtils.isNotEmpty(po.getName()), SysTenant::getName, po.getName())
                )
        );
    }

    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysTenant> get(Long id) {
        return Result.ok(sysTenantService.getOne(Wrappers.lambdaQuery(SysTenant.class).eq(SysTenant::getId, id)));
    }

    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<Long> ids) {
        return sysTenantService.delete(ids) ? Result.ok() : Result.error();
    }

    @ApiOperation("获取租户列表")
    @GetMapping("listTenants")
    public Result<List<SysTenantVO>> listTenants() {
        return Result.ok(sysTenantService.listTenants());
    }

    @ApiOperation("根据用户id、租户id，初始化新租户数据库数据")
    @PostMapping("initNewTenantDBDataByUserIdAndTenantId")
    public Result<?> initNewTenantDBDataByUserIdAndTenantId(Long userId, Long tenantId) {
        return sysTenantService.initNewTenantDBDataByUserIdAndTenantId(userId, tenantId) ? Result.ok() : Result.error();
    }

}
