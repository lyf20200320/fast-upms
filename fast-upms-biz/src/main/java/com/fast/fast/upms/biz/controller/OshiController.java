package com.fast.fast.upms.biz.controller;

import cn.hutool.system.oshi.CpuInfo;
import cn.hutool.system.oshi.OshiUtil;
import com.alibaba.fastjson.JSONObject;
import com.fast.fast.common.base.vo.Result;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import oshi.hardware.GlobalMemory;


/**
 * 系统监控Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统监控")
@RequestMapping("sysMonitor")
@RestController
public class OshiController {

    @GetMapping("getSysInfo")
    public Result<?> getSysInfo() {
        GlobalMemory memory = OshiUtil.getMemory();
        CpuInfo cpuInfo = OshiUtil.getCpuInfo();
        JSONObject jo = new JSONObject();
        jo.put("memory", memory);
        jo.put("cpuInfo", cpuInfo);
        return Result.ok(jo);
    }
}
