package com.fast.fast.upms.biz.controller;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.common.mybatis.plus.service.SuperService;
import com.fast.fast.upms.api.entity.SysLog;
import com.fast.fast.upms.biz.service.SysLogService;
import com.fast.fast.upms.biz.service.impl.SysLogServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统日志Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统日志")
@RequestMapping("sysLog")
@RestController
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    @Autowired
    private SuperService<SysLog, ?> superService;

    @ApiOperation("新增或更新")
    @PostMapping("saveOrUpdate")
    public Result<?> saveOrUpdate(@Validated SysLog po) {
        return sysLogService.saveOrUpdate(po) ? Result.ok() : Result.error();
    }

    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysLog>> page(SysLog po) {
        superService.setClazz(SysLogServiceImpl.class);
        return Result.ok(superService.page(po, Wrappers.lambdaQuery(SysLog.class)
                        .likeRight(ObjectUtils.isNotEmpty(po.getCreateBy()), SysLog::getCreateBy, po.getCreateBy())
                        .orderByDesc(SysLog::getCreateTime)
                )
        );
    }

    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysLog> get(Long id) {
        return Result.ok(sysLogService.getOne(Wrappers.lambdaQuery(SysLog.class).eq(SysLog::getId, id)));
    }

    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<?> ids) {
        return sysLogService.removeBatchByIds(ids) ? Result.ok() : Result.error();
    }

}
