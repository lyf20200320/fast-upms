package com.fast.fast.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.fast.upms.api.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lyf
 * @description 针对表【sys_role(系统角色表)】的数据库操作Mapper
 * @createDate 2022/01/01 00:00 周六
 * @Entity com.fast.fast.upms.api.entity.SysRole
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}




