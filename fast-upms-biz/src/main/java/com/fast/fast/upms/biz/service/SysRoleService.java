package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysRole;
import com.fast.fast.upms.api.vo.SysRoleVO;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_role(系统角色表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 新增或更新
     *
     * @param sysRole
     * @return
     */
    @Override
    boolean saveOrUpdate(SysRole sysRole);

    /**
     * 获取用户拥有的角色编码集合
     *
     * @param userId
     * @return
     */
    List<String> listRoleCodesByUserId(Long userId);

    /**
     * 获取角色列表
     *
     * @return
     */
    List<SysRoleVO> listRoles();

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean delete(List<?> ids);

    /**
     * 根据用户id，获取角色名称集合
     *
     * @param userId
     * @return
     */
    List<SysRole> listByUserId(Long userId);
}
