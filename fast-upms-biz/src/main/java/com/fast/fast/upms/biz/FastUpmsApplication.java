package com.fast.fast.upms.biz;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

/**
 * description
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@RefreshScope
@EnableFeignClients(basePackages = "com.fast.**")
@EnableDiscoveryClient
@Slf4j
@SpringBootApplication
public class FastUpmsApplication {

    @SneakyThrows
    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(FastUpmsApplication.class, args);
        Environment env = application.getEnvironment();
        String appName = env.getProperty("spring.application.name");
        String port = env.getProperty("server.port");
        String address = InetAddress.getLocalHost().getHostAddress();
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 '{}' 启动成功!\n\t" +
                        "本地地址: \t\thttp://localhost:{}\n\t" +
                        "局域网地址: \thttp://{}:{}\n\t" +
                        "文档地址: \thttp://{}:{}/doc.html\n" +
                        "----------------------------------------------------------",
                appName,
                port,
                address, port,
                address, port
        );
    }

}
