package com.fast.fast.upms.biz.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.common.mybatis.plus.service.SuperService;
import com.fast.fast.upms.api.entity.SysPerm;
import com.fast.fast.upms.biz.service.SysPermService;
import com.fast.fast.upms.biz.service.impl.SysPermServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统权限Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Api(tags = "系统权限")
@RequestMapping("sysPerm")
@RestController
public class SysPermController {

    @Autowired
    private SysPermService sysPermService;

    @Autowired
    private SuperService<SysPerm, ?> superService;

    private final String PERM_TREE_REDIS_KEY = "upms:sysPerm:permTree";
    private final String KEY_GENERATOR = "keyGenerator";

    @CacheEvict(value = PERM_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("新增或更新")
    @PostMapping("saveOrUpdate")
    public Result<?> saveOrUpdate(@Validated SysPerm po) {
        return sysPermService.saveOrUpdate(po) ? Result.ok() : Result.error("父节点不能是自己");
    }

    @ApiOperation("分页查询")
    @PostMapping("page")
    public Result<PageResult<SysPerm>> page(SysPerm po) {
        superService.setClazz(SysPermServiceImpl.class);
        return Result.ok(superService.page(po, Wrappers.lambdaQuery(SysPerm.class)
                        .likeRight(ObjectUtils.isNotEmpty(po.getName()), SysPerm::getName, po.getName())
                )
        );
    }

    @ApiOperation("单个查询")
    @GetMapping("get")
    public Result<SysPerm> get(Long id) {
        return Result.ok(sysPermService.getOne(Wrappers.lambdaQuery(SysPerm.class).eq(SysPerm::getId, id)));
    }

    @CacheEvict(value = PERM_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("批量删除")
    @PostMapping("delete")
    public Result<?> delete(@RequestParam List<?> ids) {
        return sysPermService.delete(ids) ? Result.ok() : Result.error();
    }

    // 若是开启多租户，则不能使用该缓存
    //@Cacheable(value = PERM_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("权限树")
    @PostMapping("permTree")
    public JSONObject permTree() {
        JSONObject rs = new JSONObject();
        rs.put("status", JSON.parse("{\"code\":200,\"message\":\"操作成功\"}"));
        rs.put("data", sysPermService.permTree());
        return rs;
    }

    @CacheEvict(value = PERM_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("添加根节点")
    @PostMapping("addRoot")
    public Result<?> addRoot(@Validated SysPerm po) {
        return sysPermService.addRoot(po) ? Result.ok() : Result.error();
    }

    @CacheEvict(value = PERM_TREE_REDIS_KEY, keyGenerator = KEY_GENERATOR)
    @ApiOperation("添加子节点")
    @PostMapping("addChild")
    public Result<?> addChild(@RequestBody JSONObject treeNode) {
        return sysPermService.addChild(treeNode) ? Result.ok() : Result.error();
    }

}
