package com.fast.fast.upms.biz.controller;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 跳转专用Controller
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@Controller
public class ForwardController {

    @RequestMapping("/")
    public String toIndex() {
        if (StpUtil.isLogin()) {
            return "/views/index.html";
        }
        return "/views/login.html";
    }
}
