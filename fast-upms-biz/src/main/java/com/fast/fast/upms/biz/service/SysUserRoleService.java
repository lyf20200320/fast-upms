package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysUserRole;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_user_role(系统用户角色关联表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     * 给用户添加角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    boolean addRoles(Long userId, List<?> roleIds);
}
