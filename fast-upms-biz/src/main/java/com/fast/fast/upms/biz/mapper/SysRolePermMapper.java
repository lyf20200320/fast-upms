package com.fast.fast.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.fast.upms.api.entity.SysRolePerm;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lyf
 * @description 针对表【sys_role_perm(系统角色权限关联表)】的数据库操作Mapper
 * @createDate 2022/01/01 00:00 周六
 * @Entity com.fast.fast.upms.api.entity.SysRolePerm
 */
@Mapper
public interface SysRolePermMapper extends BaseMapper<SysRolePerm> {

}




