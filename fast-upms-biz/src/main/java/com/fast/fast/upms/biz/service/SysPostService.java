package com.fast.fast.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.fast.upms.api.entity.SysPost;
import com.fast.fast.upms.api.vo.SysPostVO;

import java.util.List;

/**
 * @author lyf
 * @description 针对表【sys_post(系统岗位信息表)】的数据库操作Service
 * @createDate 2022/01/01 00:00 周六
 */
public interface SysPostService extends IService<SysPost> {

    /**
     * 获取岗位列表
     *
     * @return
     */
    List<SysPostVO> listPosts();

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean delete(List<?> ids);
}
