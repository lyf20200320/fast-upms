package com.fast.fast.common.mybatis.plus.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.fast.fast.common.mybatis.plus.aspect.CustomAspect;
import com.fast.fast.common.mybatis.plus.context.CustomContext;
import com.fast.fast.common.mybatis.plus.interceptor.CustomTenantInterceptor;
import com.fast.fast.common.redis.util.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * MybatisPlus配置类
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@RequiredArgsConstructor
@EnableConfigurationProperties({MyBatisPlusProperties.class})
@AutoConfiguration
public class MyBatisPlusConfig implements WebMvcConfigurer {

    @Value("${tenant.open:false}")
    private boolean tenantOpen;

    private final MyBatisPlusProperties myBatisPlusProperties;

    /**
     * MybatisPlus插件
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        if (tenantOpen) {
            // 多租户插件 注意:必须先添加多租户插件,再添加分页插件
            interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(interceptor(context())));
        }
        // 分页插件
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        // 乐观锁插件
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return interceptor;
    }

    @ConditionalOnProperty(prefix = "tenant", name = {"open"}, havingValue = "true")
    @Bean
    public CustomContext context() {
        return new CustomContext();
    }

    @ConditionalOnProperty(prefix = "tenant", name = {"open"}, havingValue = "true")
    @Bean
    public CustomTenantInterceptor interceptor(CustomContext context) {
        return new CustomTenantInterceptor(context, myBatisPlusProperties);
    }

    @ConditionalOnProperty(prefix = "tenant", name = {"open"}, havingValue = "true")
    @Bean
    public CustomAspect aspect(CustomContext context) {
        return new CustomAspect(context, myBatisPlusProperties);
    }

    @Bean
    public MybatisPlusMetaObjectHandler mybatisPlusMetaObjectHandler(RedisUtils redisUtils) {
        return new MybatisPlusMetaObjectHandler(redisUtils);
    }

    /**
     * SQL 过滤器避免SQL 注入
     *
     * @param argumentResolvers
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new SqlFilterArgumentResolver());
    }
}
