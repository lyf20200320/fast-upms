package com.fast.fast.common.mybatis.plus.context;

/**
 * MybatisPlus自定义多租户上下文
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @date 2024/09/12 17:10 周四
 **/
public class CustomContext {

    /**
     * ThreadLocal用于存储每个线程的租户ID
     */
    private static final ThreadLocal<Long> TENANT_THREAD_LOCAL = new ThreadLocal<>();

    public CustomContext() {
    }

    /**
     * 设置当前线程的租户ID
     *
     * @param tenantId 租户ID
     */
    public void setCurrentTenantId(Long tenantId) {
        TENANT_THREAD_LOCAL.set(tenantId);
    }

    /**
     * 获取当前线程的租户ID
     *
     * @return 当前线程的租户ID
     */
    public Long getCurrentTenantId() {
        return TENANT_THREAD_LOCAL.get();
    }

    /**
     * 清除当前线程的租户ID
     */
    public void clear() {
        TENANT_THREAD_LOCAL.remove();
    }

}
