package com.fast.fast.common.mybatis.plus.config;

import cn.hutool.core.util.ObjUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * MybatisPlus配置文件
 *
 * @author lyf
 * @date 2024/09/12 17:10 周四
 **/
@Data
@ConfigurationProperties(prefix = "tenant")
public class MyBatisPlusProperties {


    /**
     * yml文件参考配置
     * mybatis-plus:
     * tenant-aspect:
     * exclude-classes:
     * - com.example.SomeClass
     * - com.example.AnotherClass
     * exclude-methods:
     * - someMethod
     * - anotherMethod
     * <p>
     * tenant-interceptor:
     * exclude-tables:
     * - another_table
     * - another_table
     */

    private final TenantAspectProperties tenantAspect = new TenantAspectProperties();

    private final TenantInterceptorProperties tenantInterceptor = new TenantInterceptorProperties();

    /**
     * 多租户切面配置
     */
    @Data
    public static class TenantAspectProperties {
        /**
         * 放行的类
         */
        private Set<String> excludeClasses;

        /**
         * 放行的方法
         */
        private Set<String> excludeMethods;

        public Set<String> getExcludeClasses() {
            return Optional.ofNullable(excludeClasses)
                    .orElse(Collections.emptySet())
                    .stream()
                    .filter(ObjUtil::isNotEmpty)
                    .collect(Collectors.toSet());
        }

        public Set<String> getExcludeMethods() {
            return Optional.ofNullable(excludeMethods)
                    .orElse(Collections.emptySet())
                    .stream()
                    .filter(ObjUtil::isNotEmpty)
                    .collect(Collectors.toSet());
        }
    }

    /**
     * 多租户拦截器配置
     */
    @Data
    public static class TenantInterceptorProperties {
        /**
         * 放行的表名
         */
        private Set<String> excludeTables;
    }

}
