package com.fast.fast.common.mybatis.plus.config;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.fast.fast.common.base.constant.CacheConstants;
import com.fast.fast.common.redis.util.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Date;

/**
 * MybatisPlus自动填充类
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@RequiredArgsConstructor
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {

    private final RedisUtils redisUtils;

    /**
     * 新增一条数据时,自动填充一些字段的值
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        // 填充乐观锁版本号为0
        Object version = this.getFieldValByName("version", metaObject);
        if (ObjectUtil.isEmpty(version)) {
            this.setFieldValByName("version", 0, metaObject);
        }
        // 填充创建人为当前登录用户
        Object createBy = this.getFieldValByName("createBy", metaObject);
        if (ObjectUtil.isEmpty(createBy)) {
            // 获取登录用户的用户名
            String username = this.getUsernameByLoginId();
            if (username != null) {
                this.setFieldValByName("createBy", username, metaObject);
            }
        }
        // 填充创建时间为系统当前时间
        Object createTime = this.getFieldValByName("createTime", metaObject);
        if (ObjectUtil.isEmpty(createTime)) {
            this.setFieldValByName("createTime", new Date(), metaObject);
        }
    }

    /**
     * 更新一条数据时,自动填充一些字段的值
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        // 填充更新人为当前登录用户
        Object updateBy = this.getFieldValByName("updateBy", metaObject);
        if (ObjectUtil.isEmpty(updateBy)) {
            // 获取登录用户的用户名
            String username = this.getUsernameByLoginId();
            if (username != null) {
                this.setFieldValByName("updateBy", username, metaObject);
            }
        }
        // 填充更新时间为系统当前时间
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }

    /**
     * 获取登录用户的用户名
     *
     * @return
     */
    private String getUsernameByLoginId() {
        // 登录用户id
        Object loginId;
        try {
            loginId = StpUtil.getLoginId();
            // 获取redis中登录用户信息
            Object sysUser = redisUtils.get(CacheConstants.GATEWAY_LOGIN_SYS_USER_USERID_PREFIX + loginId);
            if (ObjectUtil.isEmpty(sysUser)) {
                return null;
            }
            JSONObject jo = JSON.parseObject(JSONObject.toJSONString(sysUser));
            // 获取登录用户名
            return jo.getString("username");
        } catch (Exception e) {
            return "";
        }
    }
}
