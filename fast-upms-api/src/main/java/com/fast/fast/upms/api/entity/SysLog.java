package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统日志表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_log
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_log")
@Data
public class SysLog extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 日志类型,默认正常(0.正常 1.异常)
     */
    private Integer type;

    /**
     * 日志标题
     */
    private String title;

    /**
     * ip
     */
    private String ip;

    /**
     * 城市
     */
    private String cname;

    /**
     * 设备
     */
    private String device;

    /**
     * 操作系统
     */
    private String platform;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 网络类型
     */
    private String networkType;

    /**
     * 服务名
     */
    private String service;

    /**
     * url
     */
    private String url;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 载荷
     */
    private String payload;

    /**
     * 响应状态
     */
    private String status;

    /**
     * 响应时长
     */
    private String time;

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 来源
     */
    private String referer;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}
