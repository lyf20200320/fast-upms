package com.fast.fast.upms.api.vo;

import com.fast.fast.common.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统角色表视图对象
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class SysRoleVO extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色编码
     */
    private String code;

    private Long value;

    private Boolean selected;

    private Boolean disabled;

    private static final long serialVersionUID = 1L;

}