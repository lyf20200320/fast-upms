package com.fast.fast.upms.api.vo;

import com.fast.fast.common.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 租户表表视图对象
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class SysTenantVO extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 租户名称
     */
    private String name;

    /**
     * 租户编码
     */
    private String code;

    private Long value;

    private Boolean selected;

    private Boolean disabled;

    private static final long serialVersionUID = 1L;

}
