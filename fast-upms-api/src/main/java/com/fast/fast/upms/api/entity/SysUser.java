package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统用户表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_user
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user")
@Data
public class SysUser extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 用户openid
     */
    @ApiModelProperty("用户openid")
    private String openid;

    /**
     * 用户unionid
     */
    @ApiModelProperty("用户unionid")
    private String unionid;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    //@NotBlank(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    //@NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 昵称
     */
    @ApiModelProperty("昵称")
    private String nickname;

    /**
     * 头像地址
     */
    @ApiModelProperty("头像地址")
    private String avatar;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String phone;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 性别,默认男(0.男 1.女)
     */
    @ApiModelProperty("性别,默认男(0.男 1.女)")
    private Integer sex;

    /**
     * 住址
     */
    @ApiModelProperty("住址")
    private String address;

    /**
     * 真实姓名
     */
    @ApiModelProperty("真实姓名")
    private String realName;

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    private String idCardNumber;

    /**
     * 账号状态,默认正常(0.正常 1.停用)
     */
    @ApiModelProperty("账号状态,默认正常(0.正常 1.停用)")
    private Integer status;

    /**
     * 最后登录ip
     */
    @ApiModelProperty("最后登录ip")
    private String lastLoginIp;

    /**
     * 最后登录时间
     */
    @ApiModelProperty("最后登录时间")
    private Date lastLoginTime;

    /**
     * 部门ID
     */
    @ApiModelProperty("部门ID")
    private Long deptId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 角色名称集合
     */
    @TableField(exist = false)
    private String roleNamesStr;

    /**
     * 角色名称集合
     */
    @TableField(exist = false)
    private List<String> roleNames;

    /**
     * 角色ids集合
     */
    @TableField(exist = false)
    private String roleIdsStr;

    /**
     * 角色ids集合
     */
    @TableField(exist = false)
    private List<String> roleIds;

}
