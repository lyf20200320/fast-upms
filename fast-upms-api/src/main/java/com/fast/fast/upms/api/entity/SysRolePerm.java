package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统角色权限关联表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_role_perm
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_role_perm")
@Data
public class SysRolePerm extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 系统角色ID
     */
    @ApiModelProperty("系统角色ID")
    private Long roleId;

    /**
     * 系统权限ID
     */
    @ApiModelProperty("系统权限ID")
    private Long permId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
