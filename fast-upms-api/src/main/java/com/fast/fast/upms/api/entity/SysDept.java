package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 系统部门表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_dept
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_dept")
@Data
public class SysDept extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 父ID,默认为父节点(0.父节点)
     */
    @ApiModelProperty("父ID,默认为父节点(0.父节点)")
    private Long pid;

    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    @NotBlank(message = "部门名称不能为空")
    private String name;

    /**
     * 部门编码
     */
    @ApiModelProperty("部门编码")
    @NotBlank(message = "部门编码不能为空")
    private String code;

    /**
     * 排序值
     */
    @ApiModelProperty("排序值")
    private Integer sort;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 子节点
     */
    @TableField(exist = false)
    private List<SysDept> children;
}
