package com.fast.fast.upms.api.feign;

import com.fast.fast.upms.api.enums.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * description
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@FeignClient(name = ServiceNameConstants.UMPS_SERVICE, path = "sysPerm")
public interface SysPermFeign {

}
