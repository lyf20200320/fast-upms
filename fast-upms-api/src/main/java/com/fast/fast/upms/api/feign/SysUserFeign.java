package com.fast.fast.upms.api.feign;

import com.fast.fast.common.base.page.PageResult;
import com.fast.fast.common.base.vo.Result;
import com.fast.fast.upms.api.entity.SysUser;
import com.fast.fast.upms.api.enums.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 系统用户Feign
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@FeignClient(name = ServiceNameConstants.UMPS_SERVICE, path = "sysUser")
public interface SysUserFeign {

    /**
     * 新增或更新
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "mySaveOrUpdate")
    Result<?> mySaveOrUpdate(@RequestBody @Validated SysUser sysUser);

    /**
     * 分页查询
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "page")
    Result<PageResult<SysUser>> page(@RequestBody SysUser sysUser);

    /**
     * 单个查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "get")
    Result<SysUser> get(@RequestParam("id") Long id);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @PostMapping(value = "delete")
    Result<?> delete(@RequestParam("ids") List<Long> ids);

    /**
     * 根据用户名和密码获取
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "getByUsernameAndPwd")
    Result<SysUser> getByUsernameAndPwd(@RequestBody SysUser sysUser);

    /**
     * 设置账号状态
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "setStatus")
    Result<?> setStatus(@RequestBody SysUser sysUser);

    /**
     * 修改个人密码
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "updatePassword")
    Result<?> updatePassword(@RequestBody SysUser sysUser);

    /**
     * 全部查询
     *
     * @param
     * @return
     */
    @GetMapping(value = "listAll")
    Result<List<SysUser>> listAll();

}
