package com.fast.fast.upms.api.vo;

import com.alibaba.fastjson.JSONObject;
import com.fast.fast.common.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 系统部门表视图对象
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class SysDeptVO extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 父ID,默认为父节点(0.父节点)
     */
    private Long parentId;

    /**
     * 部门名称
     */
    private String title;

    /**
     * 部门编码
     */
    private String code;

    /**
     * 排序值
     */
    private String sort;

    private static final long serialVersionUID = 1L;

    /**
     * 子节点
     */
    private List<SysDeptVO> children;

    /**
     * 展开
     */
    private Boolean spread;

    /**
     * 是否选中复选框
     */
    private JSONObject checkArr;

    private String name;

    private Long value;

    private Boolean selected;

    private Boolean disabled;


}