package com.fast.fast.upms.api.vo;

import com.alibaba.fastjson.JSONObject;
import com.fast.fast.common.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 系统权限表视图对象
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermVO extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 父ID,默认为父节点(0.父节点)
     */
    private Long parentId;

    /**
     * 权限名称
     */
    private String title;

    /**
     * 权限编码
     */
    private String code;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 图标
     */
    private String icon;

    /**
     * 展开,默认不展开(0.不展开 1.展开)
     */
    private Boolean spread;

    /**
     * 状态,默认启用(0.启用 1.禁用)
     */
    private Integer disabled;

    /**
     * 是否为接口,默认否(0.否 1.是)
     */
    private Integer isInterface;

    /**
     * 前端url地址
     */
    private String url;

    /**
     * 前端组件地址
     */
    private String component;

    /**
     * 是否为外部菜单，默认否(0.否 1.是)
     */
    private Integer ref;

    private static final long serialVersionUID = 1L;

    /**
     * 子节点
     */
    private List<SysPermVO> children;

    /**
     * 是否选中复选框
     */
    private JSONObject checkArr;

    private String name;

    private Long value;

    private Boolean selected;


}