package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 系统租户表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_tenant
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_tenant")
@Data
public class SysTenant extends BaseEntity implements Serializable {

    /**
     * 租户名
     */
    @ApiModelProperty("租户名")
    @NotBlank(message = "租户名不能为空")
    private String name;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}