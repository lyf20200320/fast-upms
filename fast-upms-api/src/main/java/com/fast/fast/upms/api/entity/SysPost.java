package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 系统岗位信息表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_post
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_post")
@Data
public class SysPost extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 岗位名称
     */
    @ApiModelProperty("岗位名称")
    @NotBlank(message = "岗位名称不能为空")
    private String name;

    /**
     * 岗位编码
     */
    @ApiModelProperty("岗位编码")
    @NotBlank(message = "岗位编码不能为空")
    private String code;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}
