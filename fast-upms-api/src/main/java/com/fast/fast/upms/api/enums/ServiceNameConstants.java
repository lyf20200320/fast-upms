package com.fast.fast.upms.api.enums;

/**
 * 服务名称常量
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
public interface ServiceNameConstants {

    /**
     * 通用用户权限系统
     */
    String UMPS_SERVICE = "fast-upms-biz";

}
