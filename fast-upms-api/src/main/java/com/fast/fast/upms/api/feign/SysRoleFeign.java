package com.fast.fast.upms.api.feign;

import com.fast.fast.common.base.vo.Result;
import com.fast.fast.upms.api.enums.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * description
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 **/
@FeignClient(name = ServiceNameConstants.UMPS_SERVICE, path = "sysRole")
public interface SysRoleFeign {

    /**
     * 获取用户拥有的角色编码集合
     *
     * @param userId
     * @return
     */
    @GetMapping("listRoleCodesByUserId")
    Result<List<String>> listRoleCodesByUserId(@RequestParam("userId") Long userId);

}
