package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统用户角色关联表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_user_role
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user_role")
@Data
public class SysUserRole extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 系统用户ID
     */
    @ApiModelProperty("系统用户ID")
    private Long userId;

    /**
     * 系统角色ID
     */
    @ApiModelProperty("系统角色ID")
    private Long roleId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
