package com.fast.fast.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fast.fast.common.base.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 系统权限表
 *
 * @author lyf
 * @date 2022/01/01 00:00 周六
 * @TableName sys_perm
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_perm")
@Data
public class SysPerm extends BaseEntity implements Serializable {

    /**
     * 租户id
     */
    @ApiModelProperty("租户id")
    private Long tenantId;

    /**
     * 父ID,默认为父节点(0.父节点)
     */
    @ApiModelProperty("父ID,默认为父节点(0.父节点)")
    private Long pid;

    /**
     * 权限名称
     */
    @ApiModelProperty("权限名称")
    @NotBlank(message = "权限名称不能为空")
    private String name;

    /**
     * 权限编码
     */
    @ApiModelProperty("权限编码")
    @NotBlank(message = "权限编码不能为空")
    private String code;

    /**
     * 排序值
     */
    @ApiModelProperty("排序值")
    private Integer sort;

    /**
     * 图标
     */
    @ApiModelProperty("图标")
    private String icon;

    /**
     * 展开,默认不展开(0.不展开 1.展开)
     */
    @ApiModelProperty("展开,默认不展开(0.不展开 1.展开)")
    private Integer spread;

    /**
     * 状态,默认启用(0.启用 1.禁用)
     */
    @ApiModelProperty("状态,默认启用(0.启用 1.禁用)")
    private Integer disabled;

    /**
     * 是否为接口,默认否(0.否 1.是)
     */
    @ApiModelProperty("是否为接口,默认否(0.否 1.是)")
    private Integer isInterface;

    /**
     * 前端url地址
     */
    @ApiModelProperty("前端url地址")
    private String url;

    /**
     * 前端组件地址
     */
    @ApiModelProperty("前端组件地址")
    private String component;

    /**
     * 是否为外部菜单，默认否(0.否 1.是)
     */
    @ApiModelProperty("是否为外部菜单，默认否(0.否 1.是)")
    private Integer ref;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 子节点
     */
    @TableField(exist = false)
    private List<SysPerm> children;
}
